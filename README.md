# Gitlab Runner Environments without docker machine

### Project structure

    .
    ├── api-project             # Node.js application with swapi integration
    ├── azure-kubernetes        # Guide for Gitlab Runner in Azure Kubernetes Service
    ├── hetzner-kubernetes      # Guild for Gitlab Runner in Hetzner Cloud with Kubernetes
    ├── vm-runner               # Guide for Gitlab Runner on Virtual Machine
    ├── results                 # Pipeline resutls in different enviroments
    ├── LICENSE
    └── README.md
