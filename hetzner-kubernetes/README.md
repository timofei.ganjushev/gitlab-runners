# Install Gitlab Runner via Kubernetes Operator in Hetzner

### Requirements
1. Account in [Hetzner Cloud Provider](https://www.hetzner.com/)
2. [Kubectl](https://kubernetes.io/docs/tasks/tools/) Client
3. [Homebrew](https://brew.sh/) Package Manager

### Setup Minio S3 Object storage for Cache

### Setup Kubernetes
[Terraform Kube-Hetzner Manual](https://github.com/kube-hetzner/terraform-hcloud-kube-hetzner)

### Setup Gitlab Runner
