# Install Gitlab Runner on Ubuntu
Tested on Ubuntu 22.04

### Docker installation

1. `sudo apt update`
2. Installing Docker dependencies
`sudo apt install -y ca-certificates curl gnupg lsb-release`
3. Enable Docker repository
`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg`


12. Install QEMU emulator for building multiarch images
```sudo apt-get install qemu binfmt-support qemu-user-static```

### Gitlab Runner installation
(Official Guide)(https://docs.gitlab.com/runner/install/linux-manually.html#install-1)

1. Add official gitlab repository
`curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash`
2. Install runner on Ubuntu 20.04 LTS or 22.04
`sudo apt-get install gitlab-runner`
3. Command to check GitLab Runner version
`sudo gitlab-runner -version`
4. Check Gitlab Runner status 
`sudo gitlab-runner status`
### Grant permissions to Gitlab Runner
Docker Daemon requires sudo permissions for running.
1. `cd /home`
2. `sudo visudo`
3. Added the **gitlab-runner** user to sudo group without password
`gitlab-runner ALL=(ALL:ALL) ALL`
`gitlab-runner ALL=(ALL) NOPASSWD: ALL`
   ![alt text](./images/sudo-configuration.png "Sudo configuration file")
### Register Gitlab Runner
1. Navigate to project settings -> CI/CD -> Runners and copy **URL** and registration **token**
   ![alt text](./images/gitlab-runner-registration.png "Gitlab runner registration")
2. Register the Gitlab Runner with docker executor
`sudo gitlab-runner register -n \
--url https://gitlab.com/ \
--registration-token REGISTRATION_TOKEN \
--executor docker \
--description "VM Gitlab Runner" \
--docker-image "docker:20.10.17" \
--docker-privileged \
--docker-volumes "/certs/client"`

### Uninstall Gitlab Runner
1. Removing  the Gitlab Runner using apt
`sudo apt purge --autoremove -y gitlab-runner`
2. Removing the Gitlab user and directory
`sudo deluser --remove-home gitlab-runner`
3. Removing installation directory
`sudo rm -rf /etc/gitlab-runner`
