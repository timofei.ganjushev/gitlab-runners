# Install Gitlab Runner via Kubernetes Operator
[Official documentation](https://docs.gitlab.com/runner/install/operator.html)
### Requirements
1. [Azure Account](https://azure.microsoft.com/en-us/free/)
2. [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
3. [Kubectl client](https://kubernetes.io/docs/tasks/tools/)

### Azure Cloud setup
1. Create [Azure Kubernetes Service](https://docs.microsoft.com/en-us/azure/aks/intro-kubernetes) with minimum 1 nodes (Recommended Standard_D2s_V3 instance)
2. Create [Azure Blob Storage](https://docs.microsoft.com/en-us/azure/storage/blobs/storage-blobs-introduction) Account 

### Gitlab Runner in Kubernetes
[Official Gitlab instruction](https://docs.gitlab.com/runner/install/operator.html#install-the-kubernetes-operator)

1. Update `gitlab-runner-secret.yml` with **Gitlab Runner Registration Token**.
2. Update `gitlab-runner-config-toml.yml` with **Account name, Account key, Container name**.
3. Install [Operator Livecycle Manager](https://operatorhub.io/operator/gitlab-runner-operator):
`curl -sL https://github.com/operator-framework/operator-lifecycle-manager/releases/download/v0.21.2/install.sh | bash -s v0.21.2`
4. Install the Gitlab Running Operator:
`curl -sL https://github.com/operator-framework/operator-lifecycle-manager/releases/download/v0.21.2/install.sh | bash -s v0.21.2`
5. Verify success installation:
`kubectl get csv -n operators`

_Output:_
   ![alt text](./images/operator-success-installation.png "Operator installation success")
6. Create `gitlab-runner` namespace:
`kubectl apply -f gitlab-runner-namespace.yml`
7. Create a secret containing the **Gitlab Runner Registration Token**:
`kubectl apply -f gitlab-runner-secret.yml -n gitlab-runner`
8. Create a configuration map:
`kubectl apply -f gitlab-runner-config-toml.yml -n gitlab-runner`
9. Create Custom Resource Definition (CRD) file:
`kubectl apply -f gitlab-runner.yml -n gitlab-runner`

If everything is successfully installed, the runner instance is seen in the web
![alt text](./images/available-runner.png "Available runner")

Custom configuration file could be generator from toml file with [Open Shift CLI](https://docs.openshift.com/container-platform/4.8/cli_reference/openshift_cli/getting-started-cli.html):

`oc create configmap gitlab-runner-config-toml --from-file config.toml=config.toml --dry-run=client -o yaml > gitlab-runner-config-toml.yml`
