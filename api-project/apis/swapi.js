import fetch from "node-fetch";

export const getFilms = async () => {
  const results = await fetch("https://swapi.dev/api/films");
  return await results.json();
}
