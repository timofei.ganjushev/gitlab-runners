import chai from "chai";
// starwars mocks
import starwarsFilmListMock from '../mocks/starwars/film_list.json' assert {type: 'json'};

// swapi mocks
import swapiFilmListMock from '../mocks/swapi/film_list.json' assert {type: 'json'};


const expect = chai.expect;

describe('Film List', function() {
  it('Json file reading is working', async function() {

    expect(swapiFilmListMock).to.deep.equal(starwarsFilmListMock);
  });
});
