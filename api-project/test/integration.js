import chai from 'chai'
import chaiHttp from "chai-http";
chai.use(chaiHttp);
import app from '../server.js'
const should = chai.should();
const expect = chai.expect;
// starwars mocks
import starwarsFilmListMock from '../mocks/starwars/film_list.json' assert {type: 'json'};


describe('GET /films', () => {
  it('should return a list of films when called', done => {
    chai
      .request(app)
      .get('/films')
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.body.length).to.deep.equal(6);
        done();
      });
  });
});

