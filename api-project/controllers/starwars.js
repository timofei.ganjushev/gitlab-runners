import {getFilms} from "../apis/swapi.js";


export const films = async () => {
  const swapiResponse = await getFilms();
  const filmList = [];
  swapiResponse.results.forEach(responseFilm => {
    // destructure only the fields that do not have urls in them
    let {
      characters,
      planets,
      species,
      starships,
      vehicles,
      url,
      ...film
    } = responseFilm;
    filmList.push(film);
  });
  return filmList;
};
