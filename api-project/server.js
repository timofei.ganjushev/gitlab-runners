import express from 'express'
import cors from 'cors'
import {films} from "./controllers/starwars.js";

const app = express();
const port = process.env.PORT || 1122;
app.use(cors({origin: true}));

/**
 * Passthrough to the films route in SWAPI.
 * @return {array} array of star wars films
 */
app.get('/films', async (req, res) => {
  try {
    const filmList = await films();
    res.status(200).send(filmList);
  } catch (error) {
    console.log(error.message);
    res.status(500).send(error.message);
  }

});

app.listen(port);

export default app;
